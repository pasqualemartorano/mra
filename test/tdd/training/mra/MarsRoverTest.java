package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void planetContainsObstacleAtTest() throws MarsRoverException {
		//This method tests the user story 1
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		
		boolean gotObs = mr.planetContainsObstacleAt(4, 7);
		boolean notGotObs = mr.planetContainsObstacleAt(5, 6);
		assertEquals(true, gotObs);
		assertEquals(false, notGotObs);
	}

	@Test
	public void executeNullCommandTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with null parameter
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand(null);
		assertEquals("0,0,N", gotStatus);
	}
	
	@Test
	public void executeRCommandTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with "r" parameter
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("r");
		assertEquals("(0,0,E)", gotStatus);
	}
	
	@Test
	public void executeLCommandTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with "l" parameter
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("l");
		assertEquals("(0,0,W)", gotStatus);
	}
	
	@Test
	public void executeFCommandTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with "f" parameter
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("f");
		assertEquals("(0,1,N)", gotStatus);
	}
	
	@Test
	public void executeBCommandTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with "f" parameter
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("b");
		assertEquals("(0,9,N)", gotStatus);
	}
	
	@Test
	public void executeCombinedCommandTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with a combined parameter
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"4,7","2,3"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("ffrff");
		assertEquals("(2,2,E)", gotStatus);
	}
	
	@Test
	public void executeCombinedCommandSingleObstacleTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with a combined parameter,
		//intercepting an obstacle
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"2,2"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("ffrff");
		assertEquals("(1,2,E)(2,2)", gotStatus);
	}
	
	@Test
	public void executeCombinedCommandMultipleObstaclesTest() throws MarsRoverException {
		//This method tests the mehtod MarsRover.executeCommand with a combined parameter,
		//intercepting multiple obstacles
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"2,2", "2,1"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("ffrfffrflf");
		assertEquals("(1,1,E)(2,2)(2,1)", gotStatus);
	}
	
	@Test
	public void executeCombinedCommandMultipleObstaclesWrpTest() throws MarsRoverException{
		//This method tests the mehtod MarsRover.executeCommand with a combined parameter, 
		//intercepting an obstacle, with wrapping
		int planetX = 10;
		int planetY = 10;
		List<String> obstacles = Arrays.asList(
				new String[] {"0,9"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("b");
		assertEquals("(0,0,N)(0,9)", gotStatus);
	}
	
	@Test
	public void planetTourTest() throws MarsRoverException{
		//This method tests the mehtod MarsRover.executeCommand with a combined parameter, 
		//intercepting more obstacle, with wrapping; simply, a tour around the planet
		int planetX = 6;
		int planetY = 6;
		List<String> obstacles = Arrays.asList(
				new String[] {"2,2","0,5","5,0"});
		MarsRover mr = new MarsRover(planetX, planetY, obstacles);
		String gotStatus = mr.executeCommand("ffrfffrbbblllfrfrbbl");
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", gotStatus);
	}
	
}
