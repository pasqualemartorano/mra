package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	protected int planetX;
	protected int planetY;
	protected List<String> planetObstacles;
	protected String[][] planetMatrix;
	protected String roverStatus;
	protected String landingStatus;
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.roverStatus = "0,0,N";
		this.landingStatus = "0,0,N";
		
		
		this.planetMatrix = new String[planetX][planetY];
		//fill the matrix with "X"
		for(String s : planetObstacles) {
			//coordinate x
			int xObst = Integer.parseInt(s.substring(0,1));
			//coordinate y
			int yObst = Integer.parseInt(s.substring(2));
			//assign the value "X"
			if(xObst<=planetX && yObst<=planetY && xObst>=0 && yObst>=0 && planetX>=0 && planetY>=0) {
				planetMatrix[xObst][yObst] = "X"; 
			}
			else throw new MarsRoverException("Error");
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(planetMatrix[x][y] == "X")
			return true;
		else return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String fullCommandString) throws MarsRoverException {
		if(fullCommandString == null) {
			return landingStatus;
		}
		else {
			int posX = Integer.parseInt(roverStatus.substring(0,1));
			int posY = Integer.parseInt(roverStatus.substring(2,3));
			String dir = roverStatus.substring(4);
			ArrayList<String> obstacles = new ArrayList<String>();
			char[] commStringArr = fullCommandString.toCharArray();
			for(char commandString : commStringArr) {
				int backupPosX = posX;
				int backupPosY = posY;
				if(commandString == 'r')
					dir = commandR(dir);
				if(commandString == 'l') {
					dir = commandL(dir);
				}
				
				if(commandString == 'f') {
					switch(dir) {
					case "N":
						posY+=1;
						if(posY == planetY) posY = 0;
						break;
					case "E":
						posX+=1;
						if(posX == planetX) posX = 0;
						break;
					case "S":
						posY+=-1;
						if(posY == -1) posY = (planetY-1);
						break;
					case "W":
						posX+=-1;
						if(posX == -1) posX = (planetX-1);
						break;
					default: throw new MarsRoverException("Unresolved command string");
					}
				}
				
				if(commandString == 'b') {
					switch(dir) {
					case "N":
						posY+=-1;
						if(posY == -1) posY = (planetY-1);
						break;
					case "E":
						posX+=-1;
						if(posX == -1) posX = (planetX-1);
						break;
					case "S":
						posY+=1;
						if(posY == planetY) posY = 0;
						break;
					case "W":
						posX+=1;
						if(posX == planetX) posX = 0;
						break;
					default: throw new MarsRoverException("Unresolved command string");
					}
				}
				if(planetMatrix[posX][posY]=="X") {
					String toAdd = "("+posX+","+posY+")";
					if(!obstacles.contains(toAdd)) obstacles.add(toAdd);
					posX = backupPosX;
					posY = backupPosY;
				}
			}	
			roverStatus = "("+posX+","+posY+","+dir+")";
			String obstaclesString = null;
			StringBuilder bld = new StringBuilder();
			if(obstacles.isEmpty()) {
				return roverStatus;
			}
			else {
				for(String s : obstacles)
					bld.append(s);
				obstaclesString = bld.toString();	
				return roverStatus + obstaclesString;
			}		
		}
	}
	
	public String commandR(String dir) throws MarsRoverException {
		switch(dir) {
		case "N":
			dir = "E";
			break;
		case "E":
			dir = "S";
			break;
		case "S":
			dir = "W";
			break;
		case "W":
			dir = "N";
			break;
		default: throw new MarsRoverException("Unresolved command string");
		}
		return dir;
	}
	
	public String commandL(String dir) throws MarsRoverException {
		switch(dir) {
		case "N":
			dir = "W";
			break;
		case "E":
			dir = "N";
			break;
		case "S":
			dir = "E";
			break;
		case "W":
			dir = "S";
			break;
		default: throw new MarsRoverException("Unresolved command string");
		}
		return dir;
	}
	
}
